import dash
import pandas as pd
from matplotlib import pyplot as plt
df = pd.read_csv("covid_district.csv")
week = df.WEEK
number = df.SNO
import dash_core_components as dcc
import dash_html_components as html
app = dash.Dash()
app.layout = html.Div(children=[
    html.H2('Covid - 19 Graphs'),
    dcc.Graph(figure={
                  'data': [{'x':week, 'y':number, 'type':'line', 'name':'nepal'}],
        'layout':{'title': 'Infection Growth Ratio',
                  'xaxis':{'title':'WEEK 1 - 52'},
                  'yaxis':{'title':'Number of Infections'}
                  }
    })
    ])
if __name__ == "__main__":
    app.run_server(debug=True)